#!/bin/bash

#Original work Copyright © 2017 E-One Moli Energy (Canada) Limited. All Rights Reserved.
#Modified work Copyright © 2018 David Z. Y. Lee. All Rights Reserved.
#See 'LICENCE.txt' file for details.


#WHAT THIS SCRIPT DOES AND HOW TO USE IT:
#----------------------------------------

#This script sorts the records from a mux file into separate output files, based
#on which cell/slot each record belongs to. The output files are placed in the
#same directory as the mux file. The original mux file is not altered in anyway.

#For the purpose of sorting the records from the mux file into separate output
#files, this script only needs to rely on the OVERALL cycle number, which is
#held in the second field of the record. (Read the next two sections of comments
#for what cycle numbers and overall cycle numbers are.)

#As each record is written out to its cell/slot's own output file, this script
#will convert the record's OVERALL cycle number into a "cell/slot-specific cycle
#number". The effect is to renumerate the cycle numbers in each output file, so
#that the series of cycle numbers increments by 1.


#To display information on how to use this script, run this script with the '-h'
#option.

#This script returns an exit status of:
#  0  if the program executed successfully.
#  1  if program execution is terminated because the user answered "no" to
#     deleting existing files whose names match the naming scheme that the
#     output files will use.
#  2  if the program terminated because one or more arguments supplied by the
#     user are invalid, or one or more mandatory arguments were not supplied.


#HOW THE CHARGING MACHINE, WHICH GENERATES THE MUX FILE, WORKS:
#--------------------------------------------------------------

#Cells/slots are tested in batches. Together, the records in the mux file hold
#the results of all the tests for all the cells/slots in the batch.

#Every cell/slot in the batch takes turns getting tests performed on it, and a
#single turn consists of one or more tests. When every cell/slot has taken a
#turn, one "round" of turns is completed. In the next round, every cell/slot
#takes another turn's worth of tests, and so on. The cells/slots always take
#their turns in the same sequence in every round. In the context of a mux file,
#a single turn is called a "cycle".


#HOW THE MUX FILE IS FORMATTED:
#------------------------------

#Each record in the mux file is represented by a single line that is composed of
#a series of tab-separated values. Each record holds the result of one, and only
#one, test, plus other data about that test. Since each record corresponds to
#exactly one line in the mux file, it implies that each test's data is held in
#one, and only one, line in the mux file.

#The first two lines in the mux file are NOT records. The first line contains
#information about the mux file itself. The second line is composed of a series
#of tab-separated values that represent field names (column headings, if you
#like). The field names indicate what the values in the records represent. The
#remaining lines in the mux file are all records.

#Every record (test) contains a record number in its first field. A record
#number is a sequence number that uniquely identifies a record among all the
#records in the entire mux file.

#Every record (test) also contains a "cycle (turn) number" in its second field.
#A cycle number is merely a sequence number for the cycle (turn). Since a cycle
#(turn) can consist of multiple tests, all records (tests) that belong to the
#same cycle are marked with the same cycle number.

#Since the cells/slots take turns (cycles) getting tests performed on them, the
#records for all the cells/slots in the same batch are stored in an interleaved
#fashion inside the mux file. The records are interleaved by cycles, NOT by
#tests. Furthermore, the cycles from all the cells/slots are enumerated
#collectively by one (and just that one) series of cycle numbers. This means
#that the cycle number is really an "OVERALL cycle number". "Overall" in the
#sense that the cycle number can identify, from among all tests (records) in the
#entire mux file, just those tests (records) that were performed by a particular
#cycle for a particular cell/slot.


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#Version information for this script.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#The version of this script and the release date for this version:
declare -r VERSION="1.1";
declare -r RELEASE_DATE="2018-08-29";

#The version-numbering scheme for this script is:
#    <MAJOR_NUM>.<MINOR_NUM>
#
#MINOR_NUM is incremented when the release includes only bug fixes, improvements
#to existing features, removals of existing features, or revisions to the
#comments in the script, and nothing else.
#
#MAJOR_NUM is incremented only when the release includes new features or there
#are changes that affect the contents of the output files. When MAJOR_NUM is
#incremented, reset MINOR_NUM to start counting at zero again.


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#The constants in this portion of the script allow you to alter certain
#behaviours of this program.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#This constant tells the script how many lines at the beginning of the mux file
#are header lines (i.e. lines that are NOT records):
declare -ir N_MUX_HEADER_LINES=2;

#This script generates output files and appends a cell/slot number to the end of
#the name of each file. This constant controls what the MINIMUM length in
#characters the cell/slot number will be. If the length of the cell/slot number
#is shorter than this minimum, the script will pad the number with as many
#leading zeros as necessary to make the length match this number:
declare -ir PAD_FILENUM_TO_LENGTH=3;


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#This portion of the script defines some functions for repeatedly-used
#procedures.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#Prints information on how to use this program.
#Returns a value of 0.
function funcPrintHelp ()
{
    #Format code "1" means bold font style;
    #format code "1;32" means bold font style (1) AND green colour (32); and
    #format code "0" resets all formatting to their default settings:
    echo -e "\e[1;32mUsage and Syntax:\e[0m\n"\
        "\n\e[1m$(basename -- "${0}") -h|--help\e[0m"\
        "\n   Prints this help information.\n"\
        "\n\e[1m$(basename -- "${0}") [-m] <path/to/muxFile> -n <nSlots> [-c <start>[-eof|-<end>]]\e[0m"\
        "\n   Writes out the records from a mux file into separate output files, based on which cell/slot each record belongs to. The output files are placed in the same directory as the mux file. The original mux file is not altered. The output files are named according to this naming scheme:\n"\
        "\n         <muxFileName>_cell<cellNumber>\n"\
        "\n     [-m] <path/to/muxFile>"\
        "\n         specifies the mux file to be processed, where <path/to/muxFile> is the path to the mux file. This must be specified. The optional '-m' argument explicitly tells the script that what immediately follows represents a mux file. It is only needed when the mux file has an unusual name that can be misinterpreted by the shell. For example, if the mux file is named '-c', which also happens to be an option, the '-m' argument causes the script to treat it as a file name.\n"\
        "\n     -n <nSlots>"\
        "\n         specifies how many cells/slots are in the mux file, where <nSlots> is the number. This must be specified.\n"\
        "\n     -c <start>[-eof|-<end>]"\
        "\n         specifies which cycles whose records you want to extract. This is optional. Omitting this option causes the script to extract every record, which is the default behaviour. Here is a breakdown of this option's syntax:\n"\
        "\n         -c <start>"\
        "\n             specifies a one cycle whose records you want to extract, where <start> is the overall cycle number.\n"\
        "\n         -c <start>-<end>"\
        "\n             specifies the cycles you want to start and end extraction at, and extracts the records of every cycle between, and including, the starting and ending cycles. <start> and <end> are the the overall cycle numbers of the starting and ending cycles, respectively. If <start> and <end> are equal, the result is equivalent to using '-c <start>'.\n"\
        "\n         -c <start>-eof"\
        "\n             specifies the cycle you want to start extraction at, and extracts the records of every cycle from, and including, that starting cycle to the end of the mux file. <start> is the overall cycle number of the starting cycle.\n"\
        "\n\e[1m$(basename -- "${0}") -v|--version\e[0m"\
        "\n   Prints the version of this script and the release date for that version, plus other information about this script.\n"\
        "\n\e[1;32mExit Statuses:\e[0m\n"\
        "\n   0  if the program executed successfully.\n"\
        "\n   1  if program execution is terminated because the user answered \"no\" to deleting existing files whose names match the naming scheme that the output files will use.\n"\
        "\n   2  if the program terminated because one or more arguments supplied by the user are invalid, or one or more mandatory arguments were not supplied.\n"\
        "\nThis script has been tested to work on GNU bash v4.1.10(4)-release and v4.3.48(1)-release.\n" | fmt -c -w $(tput cols) -s --;
    return 0;
}

#Prints information about this script itself, including the version number and
#the release date for the version.
#Returns a value of 0.
function funcPrintVer ()
{
    #Format code "1;35" means bold font style (1) AND magenta colour (35); and
    #format code "0" resets all formatting to their default settings:
    echo -e "$(basename -- "${0}"), \e[1;35mversion ${VERSION} (${RELEASE_DATE})\e[0m"\
        "\nThis script was tested to work on GNU bash, versions 4.1.10(4)-release and 4.3.48(1)-release, and in conjunction with the following external programs and packages:"\
        "\n  GNU sed, versions 4.2.1 and 4.2.2;"\
        "\n  GNU coreutils package, versions 8.15 and 8.25;"\
        "\n  tput (GNU ncurses package, versions 5.7 and 6.0)"\
        "\nOriginal work Copyright © 2017 E-One Moli Energy (Canada) Limited."\
        "\n  Original author: David Lee."\
        "\nModified work Copyright © 2018 David Z. Y. Lee.\n" | fmt -c -w $(tput cols) -s --;
    return 0;
}

#Formats and prints an error message that is supplied by the programmer. This
#function will automatically precede the message with the string "ERROR: ".
#It takes one argument: a string representing the error message.
#Returns a value of 0.
function funcPrintError ()
{
    #Format code "1;31" means bold font style (1) AND red colour (31); and
    #format code "0" resets all formatting to their default settings:
    echo -en "\e[1;31mERROR: ${1}\e[0m" | 1>&2 fmt -c -w $(tput cols) -s --;
    return 0;
}


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#This portion of the script parses the command-line arguments supplied by the
#user and checks if they are all valid. At minimum, the user must supply the
#path to a mux file and the number of slots in that mux file (both are mandatory
#arguments). If all arguments are valid, some of the arguments will be stored
#while the others are processed to construct program settings that will be used
#later. The script then contructs the template for the names of the output
#files. Finally it checks if there are any existing files whose names happen to
#match the naming scheme that the output files will use.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

echo "";

#Check if no command-line arguments were supplied:
if [ ${#} -eq 0 ]; then
    funcPrintError "No arguments supplied. At minimum, a mux file and the number of cells/slots the mux file contains must be specified.\n";
    funcPrintHelp;
    exit 2;
fi

#These variables will store the information parsed from the command-line
#arguments that the user supplied. The values they are initialized with here ARE
#consequential, unless stated otherwise in the comments. Some of these initial
#values can affect the behaviour of the program. Do not change these initial
#values unless you understand what they do and how they are used!
declare MUXFILE_PATHNAME="";                                        #Holds the path to the mux file. An empty string means that no mux file was specified.
declare OUT_DIR_PATH="";                                            #Holds the path to the directory that contains the mux file. The path does NOT include a trailing slash. An empty string means that no mux file was specified.
declare -i TOTAL_SLOTS=-1;                                          #Holds the number of slots in the mux file. The initial value is inconsequential.
declare OUT_FILENAMES_PREFIX="";                                    #Holds the custom prefix for the names of the output files. An empty string as the initial value informs the script that no custom prefix was specified. If the user does not supply a custom prefix, a default prefix will later be generated and assigned to this variable.
declare OUT_FILENAMES_PREFIX_PATHNAME="";                           #Holds the pathname for the prefix of the output files. Automatically generated by combining OUT_DIR_PATH + / + OUT_FILENAMES_PREFIX. The initial value is inconsequential.
declare SED_INSTR_EXTRACT_RECS="/^[1-9][0-9]*\t[1-9][0-9]*\t/p";    #Holds sed instruction that specifies which cycles to extract from the mux file. Initialized with instruction that specifies all cycles in the mux file, which is the default behaviour for this script when the user omits the '-c' option.
declare SED_INSTR_DEL_EXTRA_RECS='/^[1-9][0-9]*\t-1\t/d';           #Holds sed instruction that specifies an extraneous cycle to remove from the extracted cycles. You can end up with an extraneous cycle when the user uses the '-c <start>-<end>' option. This is because the instruction in 'SED_INSTR_EXTRACT_RECS' will automatically be set to (<end>+1), instead of <end>, for the last cycle in the range. This is necessary because of a quirk with sed's range-selection feature: sed will stop processing records after it processes the FIRST record with cycle number <end>. But because mux files can have multiple records with the same cycle number, the subsequent records will not get processed. The solution is to process one additional cycle beyond <end>, and then delete that cycle later.

#Parse the arguments supplied by the user and validate them:
while [[ ${#} -ne 0 ]]; do
    #If the -m option is specified (-m is used to state explicitly that the argument that follows immediately is a mux file):
    if [[ "${1}" =~ ^-m$ ]]; then
        shift;
        if [ -z "${1:-}" ]; then      #No argument was supplied to the -m option.
            funcPrintError "No mux file supplied to the '-m' option.\n";
            funcPrintHelp;
            exit 2;
        elif [ -d "${1}" ]; then      #The specified file is actually a directory.
# WHAT ABOUT SOCKETS, NAMED PIPEDS, CHAR DEVICES, BLOCK DEVICES?:
            funcPrintError "'$(basename -- "${1}")' is a directory. Specify a mux file and try again.\n";
            exit 2;
        elif [ ! -s "${1}" ]; then    #The specified file has a size of zero (i.e. it's empty).
            funcPrintError "The file, '$(basename -- "${1}")', is empty. Specify a different mux file and try again.\n";
            exit 2;
        elif [ ! -r "${1}" ]; then    #The specified file does not have read permission enabled on it.
            funcPrintError "The file, '$(basename -- "${1}")', is not readable. Enable read permissions on the file and try again.\n";
            exit 2;
        else                          #The argument to the -m option is valid.
            OUT_DIR_PATH="$(dirname -- "${1}")";
            MUXFILE_PATHNAME="${OUT_DIR_PATH}/$(basename -- "${1}")";    #This is necessary because the user may specify the mux file without the absolute-path or relative-path prefix.
            shift;
        fi
    #If the -n option is specified (-n is used to specify the number of slots in the mux file):
    elif [[ "${1}" =~ ^-n$ ]]; then
        shift;
        if [ -z "${1:-}" ]; then                             #No argument was supplied to the -n option.
            funcPrintError "The number of slots was not specified for the '-n' option.\n";
            funcPrintHelp;
            exit 2;
        elif [[ "${1}" =~ ^[1-9][0-9]*$ ]]; then             #The specified number is a valid number. A valid number is greater than or equal to 1, and is not padded with leading zeros.
            TOTAL_SLOTS=${1};
            shift;
        elif [[ "${1}" =~ ^((0)|(-[1-9][0-9]*))$ ]]; then    #The specified number is less than or equal to 0.
            funcPrintError "There must be at least one slot in the mux file.\n";
            exit 2;
        else                                                 #The argument supplied to the -n option is not valid at all.
            funcPrintError "The number of slots in the mux file was not correctly specified.\n";
            funcPrintHelp;
            exit 2;
        fi
    #If the -c option is specified (-c is used to specify which cycles in the mux file to process):
    elif [[ "${1}" =~ ^-c$ ]]; then
        shift;
        if [ -z "${1:-}" ]; then                                                     #No argument was supplied to the -c option.
            funcPrintError "No cycles specified for the '-c' option.\n";
            funcPrintHelp;
            exit 2;
        #This elif block generates and stores the sed instructions needed to
        #extract the specified cycles:
        elif [[ "${1}" =~ ^[1-9][0-9]*((-[eE][oO][fF])|(-[1-9][0-9]*))?$ ]]; then    #The argument supplied to the -c option is valid so far...
            if [[ ${1} =~ ^[1-9][0-9]*-[eE][oO][fF]$ ]]; then                        #<start> to EOF. Process everything starting from a specific cycle to the end of the file.
                SED_INSTR_EXTRACT_RECS='/^[1-9][0-9]*\t'${1%-*}'\t/,$p';
                #Variable already initialized to this value:
                #SED_INSTR_DEL_EXTRA_RECS='/^[1-9][0-9]*\t-1\t/d';
            elif [[ ${1} =~ ^[1-9][0-9]*-[1-9][0-9]*$ ]]; then
                if [ ${1%-*} -lt ${1#*-} ]; then                                     #<start> < <end>. Process cycles within a specific range, including "the bounds".
# USE grep INSTEAD?: grep -E '^[1-9][0-9]*'$'\t'[1-9][0-9]*$'\t'
                    SED_INSTR_EXTRACT_RECS='/^[1-9][0-9]*\t'${1%-*}'\t/,/^[1-9][0-9]*\t'$((${1#*-}+1))'\t/p';
                    SED_INSTR_DEL_EXTRA_RECS='/^[1-9][0-9]*\t'$((${1#*-}+1))'\t/d';
                elif [ ${1%-*} -eq ${1#*-} ]; then                                   #<start> == <end>. Process just one cycle, because the start and end cycles in the range are the same.
                    SED_INSTR_EXTRACT_RECS='/^[1-9][0-9]*\t'${1%-*}'\t/p';
                    #Variable already initialized to this value:
                    #SED_INSTR_DEL_EXTRA_RECS='/^[1-9][0-9]*\t-1\t/d';
                else                                                                 #<start> > <end>. This is an invalid range, because the starting cycle is larger than the ending cycle.
                    funcPrintError "The starting number in the cycle range cannot be larger than the ending number.\n";
                    exit 2;
                fi
            else # [[ ${1} =~ ^[1-9][0-9]*$ ]]                                       #Process a single cycle.
                SED_INSTR_EXTRACT_RECS='/^[1-9][0-9]*\t'${1}'\t/p';
                #Variable already initialized to this value:
                #SED_INSTR_DEL_EXTRA_RECS='/^[1-9][0-9]*\t-1\t/d';
            fi
        else                                                                         #The argument supplied to the -c option is not valid at all.
            funcPrintError "The cycle(s) were not specified correctly.\n";
            funcPrintHelp;
            exit 2;
        fi
        shift;
#UNCOMMENT THIS elif BLOCK TO ENABLE THE FEATURE THAT ALLOWS THE USER TO SPECIFY A CUSTOM FILENAME PREFIX:
#    #If the -p option is specified (-p is used to specify a custom prefix for the output files):
#    elif [[ "${1}" =~ ^-p$ ]]; then
#        shift;
#        if [ -z "${1:-}" ]; then                                          #No argument was supplied to the -p option.
#            funcPrintError "No prefix supplied to the '-p' option.\n";
#            funcPrintHelp;
#            exit 2;
#        else                                                              #There was an argument supplied to the -p option.
#            if [[ "${1}" =~ ^[_[:alnum:].][_[:alnum:]\ .-]*$ ]]; then     #The prefix is valid.
#                OUT_FILENAMES_PREFIX="${1}";
#                shift;
#            else                                                          #The prefix is not valid.
#                funcPrintError "The prefix specified is not valid. Acceptable characters for the prefix are:\n    _ . (space) - A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z 0 1 2 3 4 5 6 7 8 9\nand the first character in the prefix cannot be a space ( ) or a hypen (-).\n";
#                exit 2;
#            fi
#        fi
    #If the -h or --help option is specified:
    elif [[ "${1}" =~ ^((-h)|(--help))$ ]]; then
        funcPrintHelp;
        exit 0;
    #If the -v or --version option is specified:
    elif [[ "${1}" =~ ^((-v)|(--version))$ ]]; then
        funcPrintVer;
        exit 0;
    #If the argument is an invalid option; i.e. it begins with exactly one dash followed by a single character, or begins with exactly two dashes followed by a multi-character string:
    elif [[ "${1}" =~ (^-[^-]$)|(^--[^-]+) ]]; then
        funcPrintError "'${1}' is not a valid option.\n";
        funcPrintHelp;
        exit 2;
    #If the argument is a mux file:
    elif [[ -e "${1}" ]]; then
        if [ -d "${1}" ]; then                    #The specified file is actually a directory.
# WHAT ABOUT SOCKETS, NAMED PIPEDS, CHAR DEVICES, BLOCK DEVICES?:
            funcPrintError "'$(basename -- "${1}")' is a directory. Specify a mux file and try again.\n";
            exit 2;
        elif [ ! -s "${1}" ]; then                #The specified file has a size of zero (i.e. it's empty).
            funcPrintError "The file, '$(basename -- "${1}")', is empty. Specify a different mux file and try again.\n";
            exit 2;
        elif [ ! -r "${1}" ]; then                #The specified file does not have read permission enabled on it.
            funcPrintError "The file, '$(basename -- "${1}")', is not readable. Enable read permissions on the file and try again.\n";
            exit 2;
        else                                      #The argument is a valid mux file.
            OUT_DIR_PATH="$(dirname -- "${1}")";
            MUXFILE_PATHNAME="${OUT_DIR_PATH}/$(basename -- "${1}")";
            shift;
        fi
    #If the argument is invalid:
    else
        funcPrintError "'${1}' is neither a valid argument nor a mux file.\n";
        funcPrintHelp;
        exit 2;
    fi
done

#Check if a mux file was supplied by the user (it is a mandatory argument):
if [ -z "${MUXFILE_PATHNAME:-}" ]; then
    funcPrintError "No mux file was specified.\n";
    funcPrintHelp;
    exit 2;
fi

#Check if the total number of slots was supplied by the user (it is a mandatory argument):
if [ ${TOTAL_SLOTS} -le 0 ]; then    #Test if the number of cells in the mux file was not specified.
    funcPrintError "The number of slots/cells in the mux file was not specified.\n";
    funcPrintHelp;
    exit 2;
fi

#Check if the user provided a custom prefix for the output files:
if [ -z "${OUT_FILENAMES_PREFIX:-}" ]; then
    #A custom prefix was not supplied:
    OUT_FILENAMES_PREFIX="$(basename -- "${MUXFILE_PATHNAME}")_cell";           #Construct the default prefix.
    OUT_FILENAMES_PREFIX_PATHNAME="${OUT_DIR_PATH}/${OUT_FILENAMES_PREFIX}";    #Construct the pathname for the default prefix.
else
    #A custom prefix was supplied:
    OUT_FILENAMES_PREFIX_PATHNAME="${OUT_DIR_PATH}/${OUT_FILENAMES_PREFIX}";    #Construct the pathname for the custom prefix.
fi

#DEBUG LINES:
#echo "mux pathname: ${MUXFILE_PATHNAME:----}";
#echo "output dir pathname: ${OUT_DIR_PATH:----}";
#echo "n slots: ${TOTAL_SLOTS:----}";
#echo "prefix: ${OUT_FILENAMES_PREFIX:----}";
#echo "prefix pathname: ${OUT_FILENAMES_PREFIX_PATHNAME:----}";
#echo "sed extract instr: ${SED_INSTR_EXTRACT_RECS:----}";
#echo "sed del instr: ${SED_INSTR_DEL_EXTRA_RECS:----}";
#exit 0;

#Construct two strings. One string contains a series of this wildcard pattern:
#[0-9], and the other contains a series of hash (#) symbols. The length of each
#series is equal to the number of digits for the file numbers (this number is
#stored in the PAD_FILENUM_TO_LENGTH variable):
declare N_DIGIT_WILDCARDS="";
declare N_HASH_SYMBOLS="";
for(( i=1; i<=PAD_FILENUM_TO_LENGTH; i++ )); do
    N_DIGIT_WILDCARDS='[0-9]'"${N_DIGIT_WILDCARDS}";
    N_HASH_SYMBOLS='#'"${N_HASH_SYMBOLS}";
done

#Check if there are any existing files or directories whose names match the
#naming scheme that will be used for the output files:
declare CURRENT_FILE="";    #Temporarily holds the name or pathname of the file that is currently being operated on. This variable is later repurposed and used for another loop in the program.
for CURRENT_FILE in "${OUT_FILENAMES_PREFIX_PATHNAME}"${N_DIGIT_WILDCARDS}; do
    #If the wildcard pattern matches one or more files:
    if [ -e ${CURRENT_FILE} ]; then
        #Warn the user that there are existing files, and list those files:
        echo -e "\e[1;33mWARNING:\e[0m The output files to be generated will use the naming scheme '${OUT_FILENAMES_PREFIX}${N_HASH_SYMBOLS}', where ${N_HASH_SYMBOLS} is a ${PAD_FILENUM_TO_LENGTH}-digit number; but the same scheme is already used by these existing files and/or directories:\n" | fmt -c -w $(tput cols) -s --;
            #(ABOVE LINE) Format code "1;33" means bold font style (1) AND yellow colour (33); and format code "0" resets all formatting to their default settings.
        ls -p -C --color -d --group-directories-first -- "${OUT_FILENAMES_PREFIX_PATHNAME}"${N_DIGIT_WILDCARDS};
        echo "";
        echo -e "These files and/or directories need to be deleted before this program can proceed, but bear mind that the existing directories, if any, may contain files in them.\n" | fmt -c -w $(tput cols) -s --;

        #Ask the user repeatedly whether or not to delete those existing files and directories, until a valid "yes" or "no" response is given:
        REPLY="";
        until [[ "${REPLY}" =~ ^[ynYN]$ ]]; do
            echo -ne "Do you want to delete all the existing files and/or directories? (y/n): " | fold -w $(tput cols) -s --;
            read -r;
            echo "";
        done
        break;
    fi
# DISTINGUISH BETWEEN THE DIFFERENT TYPES OF FILES (DIRECTORIES, SYMBOLIC LINKS, SOCKETS, NAMED PIPEDS, CHAR DEVICES, BLOCK DEVICES)?
# CHECK FOR WRITE PERMISSION ON THE FILES SO THEY CAN BE DELETED?
done

#Check if the user answered "yes" to the prompt. If so, delete the existing files:
if [[ "${REPLY}" =~ ^[yY]$ ]]; then
    echo -e "Deleting:";
    rm -rfv -- "${OUT_FILENAMES_PREFIX_PATHNAME}"${N_DIGIT_WILDCARDS};
    echo "";
#CHECK IF THE DELETE WAS SUCCESSFUL?
fi

#Check if the user answered "no" to the prompt. If so, terminate the program:
if [[ "${REPLY}" =~ ^[nN]$ ]]; then
    echo -e "Move or rename the existing files and/or directories, then run this program again.\n" | fmt -c -w $(tput cols) -s --;
    exit 1;
fi


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#This portion of the script separates the records in the mux file into separate
#output files, according to the cells/slots to which the records belong.

#Each cell/slot takes turns getting one cycle's worth of tests performed it, and
#each test produces one record in the mux file. When all the cells/slots have
#each had a turn (cycle), one round of cycles is completed. In the next round of
#cycles, each cell/slot gets another turn (cycle), and so on. The cells/slots
#always take their turns in the same sequence in every round.

#Each record in the mux file stores the OVERALL cycle number of the cycle that
#its test was performed by, but it does not store information about which
#cell/slot the test was for. That latter piece of information is calculated.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#Record the time when processing begins, and display that start time:
declare START_TIME="$(date +'%F %T %z')";
echo -e "Processing started at: ${START_TIME} UTC\n" | fmt -c -w $(tput cols) -s --;

#A separate output file is generated for each cell/slot in the mux file. To
#speed up processing, a persistent file descriptor is created for each output
#file, to keep the output files open until the entire mux file is processed:
declare -ai FD_ARRAY;     #Array used to hold file-descriptor numbers. The cell/slot's number will be used as the index into the array.
declare -i CELL_NUM=0;    #Holds the number of the cell/slot currently being operated on.
declare -i CELL_FD;       #Holds the file-descriptor number for the cell/slot currently being operated on.

#Create all the output files and create a separate file descriptor for each
#output file:
for(( CELL_NUM=1; CELL_NUM<=TOTAL_SLOTS; CELL_NUM++ )); do
    exec {CELL_FD}>> $(printf "${OUT_FILENAMES_PREFIX_PATHNAME}%0${PAD_FILENUM_TO_LENGTH}d" ${CELL_NUM});    #Create the output file and an output-file descriptor, link the two, and open the file.
    FD_ARRAY[${CELL_NUM}]=${CELL_FD};    #Store the file decriptor's number in the array, at the element whose index equals the corresponding cell/slot's number.
done

#Each line in the mux file, except the first several at the head of the file, is
#a record, which is really just a series of tab-separated fields. Each record
#contains the data for one test. Since each cycle may contain multiple tests,
#there can be multiple records with the same overall cycle number:
declare -i RECORD_NUM=0;            #Holds the value in field 1 of the record currently being operated on. Field 1 contains the record's record number.
declare -i OVERALL_CYCLE_NUM=0;     #Holds the value in field 2 of the record currently being operated on. Field 2 contains the cycle's OVERALL cycle number.
declare REMAINING_FIELDS="";        #Holds the rest of the contents of the record currently being operated on.
declare -i CYCLE_NUM_FOR_CELL=0;    #This value will be calculated using the record's field 2. This variable holds the cell/slot-specific cycle number for the record currently operated.

IFS=$'\t';                          #The records use only tab characters to separate fields. Set the Internal Field Separator variable accordingly, so that the 'read' command knows what character to look for to separate fields.

#Process each record one at a time. Then write each processed record to the
#output file that corresponds to the record's cell/slot:
while read -r RECORD_NUM OVERALL_CYCLE_NUM REMAINING_FIELDS; do    #Copy field 1, field 2, and the rest of the record from the mux file to the three variables, respectively.
    CELL_NUM=$(( ${OVERALL_CYCLE_NUM} % ${TOTAL_SLOTS} ));         #Calculate which cell/slot the record belongs to.

    #Because we want the first cell/slot to always be numbered 1 (rather than
    #0), the above modulo operation will not yield the correct cell/slot number
    #whenever the cycle's overall cycle number divides evenly into the total
    #number of cells/slots (i.e. whenever the modulo operation produces a
    #remainder of 0). To correct this:
    if [ ${CELL_NUM} -eq 0 ]; then
        CELL_NUM=${TOTAL_SLOTS};
    fi

    CYCLE_NUM_FOR_CELL=$(( (${OVERALL_CYCLE_NUM}-${CELL_NUM}) / ${TOTAL_SLOTS} + 1 ));    #Calculate what the record's cycle number is for the specific cell/slot to which it belongs.
    echo -e "Writing Rec # ${RECORD_NUM} => cell ${CELL_NUM}";                            #Print a message to standard output about which output file the record is being written to.
    echo -E "${RECORD_NUM}"$'\t'"${CYCLE_NUM_FOR_CELL}"$'\t'"${REMAINING_FIELDS}" 1>&${FD_ARRAY[${CELL_NUM}]};    #Write the record to its cell/slot's output file.

       #Extract the desired records from the mux file and feed them one at a
       #time to the while-loop:
done < <( sed -nE "${SED_INSTR_EXTRACT_RECS}" "${MUXFILE_PATHNAME}" | sed -E "${SED_INSTR_DEL_EXTRA_RECS}" - )

IFS=$' \t\n';    #Restore the default value to the IFS variable.

#The header lines from the mux file need to be inserted into each output file
#that is not empty (i.e. contains records in it). At the time of this writing,
#the first line of the mux file contains information about the mux file itself
#and the second line contains the column headers for the records that follow:
declare -a MUX_HEADER_LINES;    #Array used to hold the header lines from the mux file, one line per element in the array.

#Extract the header lines in the mux file:
for(( i=1; i<=N_MUX_HEADER_LINES; i++ )); do
    MUX_HEADER_LINES[${i}]=$(sed -n ${i}p "${MUXFILE_PATHNAME}" | sed -E 's/\\/\\\\/g' -);
done

#Close all the file descriptors, delete any output files that are empty, and
#insert the header lines from the original mux file into the output files that
#are not empty:
for(( CELL_NUM=1; CELL_NUM<=TOTAL_SLOTS; CELL_NUM++ )); do
    #Close the file descriptor:
    CELL_FD=${FD_ARRAY[${CELL_NUM}]};
    exec {CELL_FD}>&-;
    unset 'FD_ARRAY['${CELL_NUM}']';    #Delete the element from the array (the actual element, not just the value in it).

    CURRENT_FILE=$(printf "${OUT_FILENAMES_PREFIX_PATHNAME}%0${PAD_FILENUM_TO_LENGTH}d" "${CELL_NUM}");    #Construct the pathname for the output file.

    #Insert the header lines into the output file if it is not empty (i.e. has
    #records in it); otherwise delete the output file:
    if [ -s "${CURRENT_FILE}" ]; then    #Does the output file have a size greater than 0?
        #The output file is not empty; insert the header lines into it:
        for(( i=1; i<=N_MUX_HEADER_LINES; i++ )); do
            sed -i "${i}i\\${MUX_HEADER_LINES[${i}]}" "${CURRENT_FILE}";
        done
    else    #The output file is empty; delete it:
        rm -f -- "${CURRENT_FILE}";
    fi
done


#Record the time when processing ended, and display the start and end times:
declare END_TIME="$(date +'%F %T %z')";
echo "";
echo "Processing started at: ${START_TIME} UTC" | fmt -c -w $(tput cols) -s --;
echo "Processing ended at:   ${END_TIME} UTC" | fmt -c -w $(tput cols) -s --;

#Calculate and display the elapsed time for the processing:
START_TIME=$(date --date="${START_TIME}" +%s);                     #Number of seconds between the 1970-01-01 00:00:00 UTC and the processing start time.
END_TIME=$(date --date="${END_TIME}" +%s);                         #Number of seconds between the 1970-01-01 00:00:00 UTC and the processing end time.

declare -i ELAPSED_SECONDS=$(( ${END_TIME}-${START_TIME} ));       #Total amount of time taken, in seconds, between start and end times.
declare -i ELAPSED_MINUTES=$(( ${ELAPSED_SECONDS}/60 ));           #Total amount of time taken, in minutes, between start and end times.

declare -i ELAPSED_HOURS=$(( ${ELAPSED_MINUTES}/60 ));             #Number of whole hours.
ELAPSED_SECONDS=$(( ${ELAPSED_SECONDS} - ${ELAPSED_MINUTES}*60 )); #Number of whole minutes in what's left in the elapsed time.
ELAPSED_MINUTES=$(( ${ELAPSED_MINUTES} - ${ELAPSED_HOURS}*60 ));   #Number of whole seconds in what's left of the elapsed time.

echo -e "Elapsed time:          ${ELAPSED_HOURS} hours ${ELAPSED_MINUTES} minutes ${ELAPSED_SECONDS} seconds\n" | fmt -c -w $(tput cols) -s --;

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

exit 0;
